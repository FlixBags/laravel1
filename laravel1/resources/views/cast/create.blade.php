@extends('layout.master')
@section('judul')
    Laman list cast
@endsection
@section('isi')
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama Cast:</label>
                <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan cast">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Umur Cast:</label>
                <input type="integer" class="form-control" name="umur" id="body" placeholder="Masukkan umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body2">Bio cast:</label>
                <input type="text" class="form-control" name="bio" id="body2" placeholder="Masukkan bio">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
@endsection
