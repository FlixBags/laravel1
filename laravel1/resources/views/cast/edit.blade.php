@extends('layout.master')
@section('judul')
    Laman edit cast ID {{$cast->id}}
@endsection
@section('isi')
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama Cast:</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="title" placeholder="Masukkan cast">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Umur Cast:</label>
                <input type="integer" class="form-control" name="umur" value="{{$cast->umur}}" id="body" placeholder="Masukkan umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body2">Umur Cast:</label>
                <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="body2" placeholder="Masukkan bio">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Ubah</button>
        </form>
@endsection
