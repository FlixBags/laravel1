@extends('layout.master')
@section('judul')
    Laman detil genre ID {{$cast->id}}
@endsection
@section('isi')
<h4>Nama cast: {{$cast->nama}}</h4>
<p>Umur cast: {{$cast->umur}}</p>
<p>Bio cast: {{$cast->bio}}</p>
@endsection