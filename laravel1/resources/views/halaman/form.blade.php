<!-- <!DOCTYPE HTML>
<html lang="en">
<head>
	<title>Buat account baru di SanberBook</title>
</head>
<body> -->
@extends('layout.master')
@section('judul')
    Laman form
@endsection
@section('isi')
	<h1>Buat account baru!</h1>
	<h3>Sign up form</h3>
	<form action="kirim" method="POST">
        @csrf
		<label>First name:</label><br>
		<input type="text" name="fname" required>
		<p>Last name:</p>
		<input type="text" name="lname" required>
		<p>Gender:</p>
		<input type="radio" name="gender" value="M">Male<br>
		<input type="radio" name="gender" value="F">Female<br>
		<input type="radio" name="gender" value="O">Other<br>
		<p>Nationality:</p>
		<select name="nat" required>
			<option value="1">Indonesian</option>
			<option value="2">American</option>
			<option value="3">British</option>
			<option value="4">Australian</option>
			<option value="5">New Zealander</option>
			<option value="6">Jamaican</option>
			<option value="7">Canadian</option>
			<option value="8">Other</option>
		</select><br>
		<p>Language spoken:</p>
		<input type="checkbox">Bahasa Indonesia<br>
		<input type="checkbox">English<br>
		<input type="checkbox">Other<br>
		<p>Bio:</p>
		<textarea name="bio" cols="30" row="30" required></textarea><br><br>
		<button type="submit">Sign Up</button>
	</form>
@endsection
<!-- </body>
</html> -->