@extends('layout.master')
@section('judul')
    Laman list genre
@endsection
@section('isi')
        <form action="/genre" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama Genre:</label>
                <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan genre">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
@endsection
