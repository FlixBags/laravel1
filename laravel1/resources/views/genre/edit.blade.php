@extends('layout.master')
@section('judul')
    Laman edit genre ID {{$genre->id}}
@endsection
@section('isi')
        <form action="/genre/{{$genre->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama Genre:</label>
                <input type="text" class="form-control" name="nama" value="{{$genre->nama}}" id="title" placeholder="Masukkan genre">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Ubah</button>
        </form>
@endsection
