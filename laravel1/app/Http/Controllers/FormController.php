<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function bio(){
        return view('halaman.form');
    }
    public function kirim(Request $request){
        $fname=$request['fname'];
        $lname=$request['lname'];
        return view('halaman.home', compact('fname','lname'));
    }
}
